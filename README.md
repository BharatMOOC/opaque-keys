Part of `BharatMOOC code`_.

.. _`BharatMOOC code`: http://code.bharatmooc.org/

opaque-keys  |build-status| |coverage-status|
=============================================

Opaque-keys is an API used by bharatmooc-platform to construct database keys.
"Opaque keys" are used where the application should be agnostic with
respect to key format, and uses an API to get information about the key
and to construct new keys.

See the `bharatmooc-platform wiki documentation`_ for more detail.

.. |build-status| image:: https://travis-ci.org/bharatmooc/opaque-keys.png  
   :target: https://travis-ci.org/bharatmooc/opaque-keys
.. |coverage-status| image:: https://coveralls.io/repos/bharatmooc/opaque-keys/badge.png
   :target: https://coveralls.io/r/bharatmooc/opaque-keys
.. _`bharatmooc-platform wiki documentation`: https://github.com/bharatmooc/bharatmooc-platform/wiki/Opaque-Keys
